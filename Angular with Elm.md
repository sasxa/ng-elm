# Initializing Elm program

> Elm exposes some user modules in JavaScript and they can be accessed through `Elm` global variable. We can access `Elm.App` because there is a `App.elm` file somewhere in the project and we told Elm to export it.

When `Elm.App` is a [`Html.program`](http://package.elm-lang.org/packages/elm-lang/html/latest/Html#program) it can be displayed on the page.

## `Elm.App.embed(<HOST>)`
When `Elm.App.embed(<HOST>)` is used, app is embedded into `<HOST>` tag. Host tag can be a custom element, for example `<elm-root>`.

`index.html`:
```html
<body>
    <app-root></app-root>
    <elm-root></elm-root>
</body>
```

`main.ts`:
```js
declare const Elm;

const elmRoot = document.querySelector('elm-root');
const elmApp = Elm.App.embed(elmRoot);
```

![Embedding Elm app with JavaScript](./src/assets/images/01.gif)

## `Elm.App.fullscreen()`
When `Elm.App.fullscreen()` is used app is inserted into DOM as last child of the body tag.

`index.html`:
```html
<body>
    <app-root></app-root>
    <elm-root></elm-root>
</body>
```

`main.ts`:
```js
declare const Elm;

const elmApp = Elm.App.fullscreen();
```

![Full screen Elm app with JavaScript](./src/assets/images/02.gif)

`index.html`:
```html
<body>
    <app-root></app-root>
    <elm-root hidden></elm-root>
</body>
```

![Full screen Elm app with JavaScript](./src/assets/images/03.gif)

## Angular Directive
`Elm.App` can be inserted into Angular in similar way using a directive.

`index.html`:
```html
<body>
    <app-root></app-root>
    <elm-root></elm-root>
</body>
```

`main.ts`:
```js
declare const Elm;

const elmRoot = document.querySelector('elm-root');
const elmApp = Elm.App.embed(elmRoot);
```

`src/app/elm.directive.ts`:
```ts
import { Directive, ElementRef, OnInit } from '@angular/core';

declare const Elm;

@Directive({ selector: '[elm]' })
export class ElmDirective implements OnInit {
    host: HTMLElement;

    constructor(private eref: ElementRef) {
        this.host = this.eref.nativeElement;
    }

    ngOnInit() {
        Elm.App.embed(this.host);
    }

}
```

![Embedding Elm app with Angular](./src/assets/images/04.gif)

When `Elm.App` is a [`Platform.program`](http://package.elm-lang.org/packages/elm-lang/core/latest/Platform#program) it cannot be displayed on the page. Instead it can be used as a worker.

## `Elm.Worker.worker()`

`src/app/elm-worker.service.ts`:
```ts
import { Injectable } from '@angular/core';

declare const Elm;

@Injectable()
export class ElmWorkerService {
    private worker = Elm.Worker.worker();
}
```

# Communicating with Elm program

> Comunication with Elm programs is accomplished via it's ***ports*** as described [in documentation](https://guide.elm-lang.org/interop/javascript.html).

I created two `ports` **fromAngular** and **toAngular**, first to send commands from Angular to Elm, and second to receive replies from Elm in Angular.

Sending actions and receiving messages is done via `ElmWorkerService`:


`src/app/elm-worker.service.ts`:
```ts
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

declare const Elm;

@Injectable()
export class ElmWorkerService {

    private worker = Elm.Worker.worker();

    messages$ = Observable.create(observer => {

        this.worker.ports.toAngular.subscribe(message =>
            observer.next(message));

        return this.worker.ports.toAngular.unsubscribe;
    });

    dispatch(action: string) {
        this.worker.ports.fromAngular.send(action);
    }
}
```

`src/app/app-component.ts`:
```ts
import { Component } from '@angular/core';
import { ElmWorkerService } from './elm-worker.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent {

    constructor(public elm: ElmWorkerService) { }

}
```

`src/app/app-component.html`:
```html
<div>
    <h2>Elm Service!</h2>
    <input (input)="elm.dispatch($event.target.value)">
    <p *ngIf="elm.messages$ |async as uppercase">from Elm: {{ uppercase }}</p>
</div>
```

In this example I am sending lowercase text from Angular App to Elm App, and receiving uppercase text.

![Angular and Elm communicating](./src/assets/images/05.gif)

# State management with Elm

`src/app/elm-store.service.ts`:
```ts
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { map } from 'rxjs/operators';

declare const Elm;

@Injectable()
export class ElmStoreService {
    private worker = Elm.Main.worker();

    state: Observable<any> = Observable.create(observer => {

        this.worker.ports.change.subscribe(data =>
        observer.next(data));

        return this.worker.ports.change.unsubscribe;
    });

    dispatch(action: string, payload = null) {
        this.worker.ports.action.send([ action, payload ]);
    }

    /**
    * Basic implementation of Store.select() from @ngrx/store:
    *
    * https://github.com/ngrx/platform/blob/master/modules/store/src/store.ts#L25-L69
    *
    */
    select(selector: string | Function, ...rest: string[]) {
        const mapFn = typeof selector === 'function'
            ? map(state => selector(state))
            : map(state =>
                [ selector, ...rest ].reduce((dict, key) =>
                    dict[ key ] || {}, state));

        return this.state.pipe(mapFn);
    }
}
```

`src/app/app-component.ts`:
```ts
import { Component } from '@angular/core';
import { ElmStoreService } from './elm-store.service';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent {
    items$: Observable<any> = this.store.select('items');

    constructor(public store: ElmStoreService) { }

}
```

`src/app/app-component.html`:
```html
<div>
    <h2>Elm Store!</h2>

    <div>
        <button (click)="store.dispatch('Decrement')">Remove</button>
        <button (click)="store.dispatch('Increment')">Add</button>
    </div>

    <ng-container *ngIf="items$ |async as items">
        <ul>
            <li *ngFor="let item of items"></li>
        </ul>
        <p>Items in store: {{ items?.length }}</p>
    </ng-container>
</div>
```

![Angular with Elm Store](./src/assets/images/06.gif)

import { AppComponent } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { ElmDirective } from './elm.directive';
import { NgModule } from '@angular/core';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { ElmWorkerService } from './elm-worker.service';
import { ElmStoreService } from './elm-store.service';

@NgModule({
  declarations: [
    AppComponent,
    ElmDirective
  ],
  imports: [
    BrowserModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [ ElmStoreService, ElmWorkerService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }

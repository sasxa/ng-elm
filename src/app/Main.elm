port module Main exposing (Model, Msg, update, subscriptions, init)


port action : (Action -> msg) -> Sub msg


port change : Model -> Cmd msg


port effect : String -> Cmd msg


main : Program Never Model Msg
main =
    Platform.program
        { init = init
        , update = update
        , subscriptions = subscriptions
        }


init : ( Model, Cmd Msg )
init =
    ( Model [], Cmd.none )


type alias Action =
    ( String, Maybe Payload )


type alias Payload =
    String


type alias Model =
    { items : List String
    }


type Msg
    = Reducer Action
    | Change
    | Increment
    | Decrement


reducer : Action -> Msg
reducer action =
    let
        -- x = Debug.log "REDUCER: model" model
        y =
            Debug.log "REDUCER: action" action
    in
        case Tuple.first action of
            "Increment" ->
                Increment

            "Decrement" ->
                Decrement

            _ ->
                Change


subscriptions : Model -> Sub Msg
subscriptions model =
    action Reducer


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Reducer a ->
            -- create Msg from IN port data
            update (reducer a) model

        Change ->
            -- send model through OUT port
            ( model, change model )

        {-
           Increment and Decrement
           do the same thing...
        -}
        Increment ->
            { model | items = "" :: model.items }
                |> update Change

        Decrement ->
            update Change <| { model | items = List.drop 1 model.items }

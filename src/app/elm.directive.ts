import { Directive, ElementRef, OnInit } from '@angular/core';

declare const Elm;

@Directive({ selector: '[elm]' })
export class ElmDirective implements OnInit {
  host: HTMLElement;

  constructor(private eref: ElementRef) {
    this.host = this.eref.nativeElement;
  }

  ngOnInit() {
    Elm.App.embed(this.host);
    // Elm.App.fullscreen();
  }

}
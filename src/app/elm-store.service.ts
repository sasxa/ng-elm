import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { map } from 'rxjs/operators';

declare const Elm;

@Injectable()
export class ElmStoreService {
  private worker = Elm.Main.worker();

  state: Observable<any> = Observable.create(observer => {

    this.worker.ports.change.subscribe(data =>
      observer.next(data));

    return this.worker.ports.change.unsubscribe;
  });

  dispatch(action: string, payload = null) {
    this.worker.ports.action.send([ action, payload ]);
  }

  /**
   * Basic implementation of Store.select() from @ngrx/store:
   * 
   * https://github.com/ngrx/platform/blob/master/modules/store/src/store.ts#L25-L69
   * 
   */
  select(selector: string | Function, ...rest: string[]) {
    const mapFn
      = typeof selector === 'function'
        ? map(state => selector(state))
        : map(state =>
          [ selector, ...rest ].reduce((dict, key) =>
            dict[ key ] || {}, state));

    return this.state.pipe(mapFn);
  }
}
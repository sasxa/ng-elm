module App exposing (Model, Msg, update, view, subscriptions, init)

import Html exposing (..)
import Html.Attributes exposing (..)


main : Program Never Model Msg
main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


type alias Model =
    { property : String
    }


type Msg
    = A
    | B


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        A ->
            ( model, Cmd.none )

        B ->
            ( model, Cmd.none )


elmLogoDataImage : String
elmLogoDataImage =
    "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE3LjEuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPgo8IURPQ1RZUEUgc3ZnIFBVQkxJQyAiLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4iICJodHRwOi8vd3d3LnczLm9yZy9HcmFwaGljcy9TVkcvMS4xL0RURC9zdmcxMS5kdGQiPgo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IgogICB2aWV3Qm94PSIwIDAgMzIzLjE0MSAzMjIuOTUiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDMyMy4xNDEgMzIyLjk1IiB4bWw6c3BhY2U9InByZXNlcnZlIj4KPGc+CiAgPHBvbHlnb24KICAgIGZpbGw9IiNGMEFEMDAiCiAgICBwb2ludHM9IjE2MS42NDksMTUyLjc4MiAyMzEuNTE0LDgyLjkxNiA5MS43ODMsODIuOTE2Ii8+CgogIDxwb2x5Z29uCiAgICBmaWxsPSIjN0ZEMTNCIgogICAgcG9pbnRzPSI4Ljg2NywwIDc5LjI0MSw3MC4zNzUgMjMyLjIxMyw3MC4zNzUgMTYxLjgzOCwwIi8+CgogIDxyZWN0CiAgICBmaWxsPSIjN0ZEMTNCIgogICAgeD0iMTkyLjk5IgogICAgeT0iMTA3LjM5MiIKICAgIHRyYW5zZm9ybT0ibWF0cml4KDAuNzA3MSAwLjcwNzEgLTAuNzA3MSAwLjcwNzEgMTg2LjQ3MjcgLTEyNy4yMzg2KSIKICAgIHdpZHRoPSIxMDcuNjc2IgogICAgaGVpZ2h0PSIxMDguMTY3Ii8+CgogIDxwb2x5Z29uCiAgICBmaWxsPSIjNjBCNUNDIgogICAgcG9pbnRzPSIzMjMuMjk4LDE0My43MjQgMzIzLjI5OCwwIDE3OS41NzMsMCIvPgoKICA8cG9seWdvbgogICAgZmlsbD0iIzVBNjM3OCIKICAgIHBvaW50cz0iMTUyLjc4MSwxNjEuNjQ5IDAsOC44NjggMCwzMTQuNDMyIi8+CgogIDxwb2x5Z29uCiAgICBmaWxsPSIjRjBBRDAwIgogICAgcG9pbnRzPSIyNTUuNTIyLDI0Ni42NTUgMzIzLjI5OCwzMTQuNDMyIDMyMy4yOTgsMTc4Ljg3OSIvPgoKICA8cG9seWdvbgogICAgZmlsbD0iIzYwQjVDQyIKICAgIHBvaW50cz0iMTYxLjY0OSwxNzAuNTE3IDguODY5LDMyMy4yOTggMzE0LjQzLDMyMy4yOTgiLz4KPC9nPgo8L3N2Zz4K"


viewHeader : Html msg
viewHeader =
    div [ style [ ( "text-align", "center" ) ] ]
        [ h1 []
            [ text "Elm App!" ]
        , img
            [ width 100, src elmLogoDataImage ]
            []
        ]


view : Model -> Html Msg
view model =
    viewHeader


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


init : ( Model, Cmd Msg )
init =
    ( Model "Hello 2", Cmd.none )

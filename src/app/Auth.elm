module Auth exposing (Msg)


type alias User =
    String


type alias State =
    { status : String
    , user : Maybe User
    }


initialiState : State
initialiState =
    { status = "Not Authenticated"
    , user = Nothing
    }


type Msg
    = Authenticated User
    | NotAuthenticated


type Effect
    = Verify -- UI Action -> query backend
    | Error -- backend Action
      -- these should be API actions ??
    | Login -- UI Action
    | Logout -- UI Action


update : Msg -> State -> ( State, Cmd Msg )
update msg state =
    case msg of
        Authenticated user ->
            { state | status = "Authenticated", user = Just user }
                |> \state -> ( state, Cmd.none )

        NotAuthenticated ->
            { state | status = "Not Authenticated", user = Nothing }
                |> \state -> ( state, Cmd.none )



-- Error ->
--     state |> update NotAuthenticated
-- Login ->
--     state |> update NotAuthenticated
-- Logout ->
--     state |> update NotAuthenticated
-- Verify ->
--     { state | status = "Pending" }
--         |> \state -> ( state, Cmd.none )

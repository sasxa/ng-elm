import { Component } from '@angular/core';
import { ElmWorkerService } from './elm-worker.service';
import { ElmStoreService } from './elm-store.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  items$: Observable<any> = this.store.select('items');

  constructor(
    public elm: ElmWorkerService,
    public store: ElmStoreService) { }
  
}

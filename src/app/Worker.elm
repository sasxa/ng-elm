port module Worker exposing (Model, Msg, update, subscriptions, init)

import String


main : Program Never Model Msg
main =
    Platform.program
        { init = init
        , update = update
        , subscriptions = subscriptions
        }


type alias Model =
    { property : String
    }


type Msg
    = A String
    | B String



-- port for sending messages to JavaScript


port toAngular : String -> Cmd msg



-- port for listening for messages from JavaScript


port fromAngular : (String -> msg) -> Sub msg


test : String -> Msg -> Model -> Msg
test a msg model =
    let
        x =
            Debug.log "action" msg
    in
        B (String.toUpper a)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        A action ->
            update (B (String.toUpper action)) model

        B a ->
            ( model, toAngular a )


subscriptions : Model -> Sub Msg
subscriptions model =
    fromAngular A


init : ( Model, Cmd Msg )
init =
    ( Model "Hello 2", Cmd.none )

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

declare const Elm;

@Injectable()
export class ElmWorkerService {
  private worker = Elm.Worker.worker();

  messages$ = Observable.create(observer => {

    this.worker.ports.toAngular.subscribe(message =>
      observer.next(message));

    return this.worker.ports.toAngular.unsubscribe;
  });

  dispatch(action: string) {
    this.worker.ports.fromAngular.send(action);
  }
}
